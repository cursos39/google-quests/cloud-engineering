# Cloud Engineering: Challenge Lab

The url of the lab is [here][1].

## Overview

Topics tested:

* Creating and using VPCs and subnets
* Configuring and launching a Deployment Manager configuration
* Creating a Kubernetes cluster
* Configuring and launching a Kubernetes deployment and service
* Setting up stackdriver monitoring
* Configuring an IAM role for an account

## Challenge scenario

You need to complete the following tasks:

* Create a development VPC with three subnets manually
* Create a production VPC with three subnets using a provided Deployment Manager configuration
* Create a bastion that is connected to both VPCs
* Create a development Cloud SQL Instance and connect and prepare the WordPress environment
* Create a Kubernetes cluster in the development VPC for WordPress
* Prepare the Kubernetes cluster for the WordPress environment
* Create a WordPress deployment using the supplied configuration
* Enable monitoring of the cluster via stackdriver
* Provide access for an additional engineer

Some Jooli Inc. standards you should follow:

* Create all resources in the us-east1 region and us-east1-b zone, unless otherwise directed.
* Use the project VPCs.
* Naming is normally team-resource, e.g. an instance could be named kraken-webserver1.
* Allocate cost effective resource sizes. Projects are monitored and excessive resource use will result in the containing project's termination (and possibly yours), so beware. This is the guidance the monitoring team is willing to share: unless directed, use n1-standard-1.

### Your challenge

You need to help the team with some of their initial work on a new project. They plan to use WordPress and need you to set up a development environment. Some of the work was already done for you, but other parts require your expert skills.

#### Environment

![Environment](../resources/environment.png)

#### Task 1: Create development VPC manually

Create a VPC called griffin-dev-vpc with the following subnets only:

* griffin-dev-wp
    * IP address block: 192.168.16.0/20
* griffin-dev-mgmt
    * IP address block: 192.168.32.0/20

```sh
gcloud compute networks create griffin-dev-vpc --subnet-mode=custom

gcloud compute networks subnets create griffin-dev-wp --network=griffin-dev-vpc --region=us-east1 --range=192.168.16.0/20

gcloud compute networks subnets create griffin-dev-mgmt --network=griffin-dev-vpc --region=us-east1 --range=192.168.32.0/20
```

#### Task 2: Create production VPC using Deployment Manager

Use Cloud Shell and copy all files from `gs://cloud-training/gsp321/dm`.

Check the Deployment Manager configuration and make any adjustments you need, then use the template to create the production VPC with the 2 subnets.

```sh
gsutil cp -r gs://cloud-training/gsp321/dm .
gcloud deployment-manager deployments create griffin-configuration --config prod-network.yaml
```

#### Task 3: Create bastion host

Create a bastion host with two network interfaces, one connected to griffin-dev-mgmt and the other connected to griffin-prod-mgmt. Make sure you can SSH to the host.

```sh
gcloud beta compute instances create griffin-bastion --zone=us-east1-b --machine-type=n1-standard-1 --subnet=griffin-dev-mgmt --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=288634840552-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --image=debian-10-buster-v20200413 --image-project=debian-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=griffin-bastion --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any

gcloud compute firewall-rules create griffin-dev-vpc-allow-ssh --description=griffin-dev-vpc-allow-ssh --direction=INGRESS --priority=1000 --network=griffin-dev-vpc --action=ALLOW --rules=tcp:22 --source-ranges=0.0.0.0/0

gcloud compute firewall-rules create griffin-prod-vpc-allow-ssh --description=griffin-prod-vpc-allow-ssh --direction=INGRESS --priority=1000 --network=griffin-prod-vpc --action=ALLOW --rules=tcp:22 --source-ranges=0.0.0.0/0
```

#### Task 4: Create and configure Cloud SQL Instance

Create a MySQL Cloud SQL Instance called griffin-dev-db in us-east1. Connect to the instance and run the following SQL commands to prepare the WordPress environment:

```sh
CREATE DATABASE wordpress;
GRANT ALL PRIVILEGES ON wordpress.* TO "wp_user"@"%" IDENTIFIED BY "stormwind_rules";
FLUSH PRIVILEGES;
```

**Solution**

```sh
gcloud sql connect  griffin-dev-db --user=root
```

#### Task 5: Create Kubernetes cluster

Create a 2 node cluster (n1-standard-4) called griffin-dev, in the griffin-dev-wp subnet, and in zone us-east1-b.

```sh
gcloud beta container clusters create "griffin-dev" --zone "us-east1-b" --no-enable-basic-auth --cluster-version "1.14.10-gke.36" --machine-type "n1-standard-4" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --num-nodes "2" --enable-stackdriver-kubernetes --enable-ip-alias --network "projects/qwiklabs-gcp-03-c0a5c76668a5/global/networks/griffin-dev-vpc" --subnetwork "projects/qwiklabs-gcp-03-c0a5c76668a5/regions/us-east1/subnetworks/griffin-dev-wp" --default-max-pods-per-node "110" --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0
```

#### Task 6: Prepare the Kubernetes cluster

Use Cloud Shell and copy all files from `gs://cloud-training/gsp321/wp-k8s`.

The WordPress server needs to access the MySQL database using the username and password you created in task 4. You do this by setting the values as secrets. WordPress also needs to store its working files outside the container, so you need to create a volume.

Add the following secrets and volume to the cluster using wp-env.yaml. Make sure you configure the username to wp_user and password to stormwind_rules before creating the configuration.

You also need to provide a key for a service account that was already set up. This service account provides access to the database for a sidecar container. Use the command below to create the key, and then add the key to the Kubernetes environment.

```sh
gcloud iam service-accounts keys create key.json \
    --iam-account=cloud-sql-proxy@$GOOGLE_CLOUD_PROJECT.iam.gserviceaccount.com
kubectl create secret generic cloudsql-instance-credentials \
    --from-file key.json
```

**Solution**    

```sh
gsutil cp -r gs://cloud-training/gsp321/wp-k8s .
# Edit the wp-env.yaml to include the user and password
kubectl apply -f wp-env.yaml
```

#### Task 7: Create a WordPress deployment

Now you have provisioned the MySQL database, and set up the secrets and volume, you can create the deployment using wp-deployment.yaml. Before you create the deployment you need to edit wp-deployment.yaml and replace YOUR_SQL_INSTANCE with griffin-dev-db's Instance connection name. Get the Instance connection name from your Cloud SQL instance.

After you create your WordPress deployment, create the service with wp-service.yaml.

Once the Load Balancer is created, you can visit the site and ensure you see the WordPress site installer. At this point the dev team will take over and complete the install and you move on to the next task.

**Solution**

SQL_INSTANCE_NAME=qwiklabs-gcp-03-c0a5c76668a5:us-east1:griffin-dev-db

```sh
vi wp-deployment.yaml # Edit the SQL_INSTANCE_NAME
kubectl apply -f wp-deployment.yaml
kubectl apply -f wp-service.yaml
```

#### Task 8: Enable monitoring

Create an uptime check for your WordPress development site.

**Solution**

Create an uptime check by TCP (ip + port 80) named `griffin-wp-uptime-check`.

#### Task 9: Provide access for an additional engineer

You have an additional engineer starting and you want to ensure they have access to the project, so please go ahead and grant them the editor role to the project.

The second user account for the lab represents the additional engineer.

**Solution**

Include the user in the IAM page.

## Congratulations!

[1]: https://google.qwiklabs.com/focuses/10603?parent=catalog