# Deployment Manager - Full Production

The link for the lab is [here][1].

## Overview

## Objectives

## Setup and requirements

```sh
gcloud auth list
gcloud config list project
```

## Create a virtual environment

```sh
sudo apt-get update
sudo apt-get install virtualenv
virtualenv -p python3 venv
source venv/bin/activate
```

## Clone the deployment manager sample templates

```sh
mkdir ~/dmsamples
cd ~/dmsamples
git clone https://github.com/GoogleCloudPlatform/deploymentmanager-samples.git
```

## Explore the sample files

```sh
cd ~/dmsamples/deploymentmanager-samples/examples/v2
ls
cd nodejs/python
```

![Schema of Deployment](../resources/deployment-manager-schema.png)

## Customize the deployment

```sh
gcloud compute zones list # got asia-southeast1-b
nano nodejs.yaml
```

## Run the application

```sh
gcloud deployment-manager deployments create advanced-configuration --config nodejs.yaml
```

## Verify that the application is operational

```sh
gcloud compute forwarding-rules list
# Go To http://<your IP address>:8080 --> in this case: http://35.186.159.198:8080
```

```properties
FORWARDING_URL=35.186.159.198
```

Go to `http://35.186.159.198:8080/?msg=example%20message`
Go ahead and create more logs and see them at `http://35.186.159.198:8080`

## Configure the uptime check and alert policy in Cloud Monitoring

## Configure an alerting policy and notification

## Configure a dashboard with a couple of useful charts

## Create a VM with Apache Bean

```sh
# Inside the new VM reciently created with SSH
sudo apt-get update
sudo apt-get -y install apache2-utils
ab -n 1000 -c 100 http://35.186.159.198:8080/
ab -n 1000 -c 100 http://35.186.159.198:8080/
ab -n 1000 -c 100 http://35.186.159.198:8080/
# Wait two minutes
ab -n 5000 -c 100 http://35.186.159.198:8080/
ab -n 5000 -c 100 http://35.186.159.198:8080/
ab -n 5000 -c 100 http://35.186.159.198:8080/
# Wait two minutes
ab -n 10000 -c 100 http://35.186.159.198:8080/
ab -n 10000 -c 100 http://35.186.159.198:8080/
ab -n 10000 -c 100 http://35.186.159.198:8080/
```

Now we lower the CPU usage per instance to 20% in the instance group.

```sh
ab -n 10000 -c 100 http://35.186.159.198:8080/
ab -n 10000 -c 100 http://35.186.159.198:8080/
ab -n 10000 -c 100 http://35.186.159.198:8080/
```

**Expected behavior**: The load consumed more than 20% of the cumulative CPU in the group, triggering autoscaling. A new instance was started.

Now see what happens when you turn autoscaling off.

```sh
ab -n 10000 -c 100 http://35.186.159.198:8080/
ab -n 10000 -c 100 http://35.186.159.198:8080/
ab -n 10000 -c 100 http://35.186.159.198:8080/
```

**Expected behavior**: With autoscaling off, no new instances are created, cumulative CPU usage increases.
There is a lag time of around 5 minutes before you see changes in the Cloud Monitoring Dashboard.

## Simulate a Service Outage

To simulate an outage, remove the firewall.


## Test your knowledge

## Congratulations!

## Finish your Quest



[1]: https://google.qwiklabs.com/focuses/981?parent=catalog